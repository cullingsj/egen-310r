# README #

This repository contains the source code, .apk, and .aia files for the Arduino and Control app for group B.1 for EGEN 310R in the Fall 2018 semester. The code is posted for REVIEW PURPOSES ONLY by the staff of Montana State University. It is not for personal use or to be used for any future assignment submission.

These such files may be found under downloads in the menu on the left.